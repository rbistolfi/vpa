=======
PVC API
=======


The packages.vectorlinux.com API.


::

   GET /
   
   API description and entry points


::
   
   GET /<release>

   Return the available architechtures for <release>


::

   GET /<release>/<arch>

   Return the available repositories for the given platform


::

   GET /<release>/<arch>/<repository>

   Return all the available packages for the repository


::

   GET /<release>/<arch>/<repository>/PACKAGES.TXT

   Return package data in plain text format


::

   GET /<release>/<arch>/<repository>/<category>/

   Return available packages in the given category


::

   GET /<release>/<arch>/<repository>/<category>/<package>

   Return package data


::

   POST /<release>/<arch>/<repository>/<category>/<package>
   { ... package data ... }

   Create a new package


::

   POST /<release>/<arch>/<repository>/<category>/<package>
   { ... package data ... }

   Update package data


::

   DELETE /<release>/<arch>/<repository>/<category>/<package>

   Delete a package


The Package data type
=====================

A *package* object looks like this:

::

   {
       "Arch": "i586",
       "Build": 3,
       "Description": "Document oriented database",
       "Category": "development",
       "Name": "CouchDB",
       "Repository": "extra",
       "Required": "erlang-otp >= R14, openssl",
       "Conflicts": "",
       "Suggests": "",
       "Release": "7.1",
       "Version": "1.3.0",
       "Filename": "CouchDB-1.3.0-i586-3vl71.txz",
       "Url":, "http://mirror.vl.com/extra/development/",
       "Md5": "2e1d0773931627e23809b1d0ba52035e",
       "SizeCompressed": 123KB,
       "SizeUncompressed": 1234KB
   }
