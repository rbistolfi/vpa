require "rubygems"
require "sinatra"
require "erb"
require "json"
require "redis"


get "/" do
    {
        "releases" => ["7.0", "7.1"]
    }.to_json
end


get "/:release" do
    {
        "arch" => ["x86", "x86_64"]
    }.to_json
end


get "/:release/:arch" do
    {
        "repos" => ["packages", "extra", "testing", "patches", "attic"]
    }.to_json
end


get "/:release/:arch/:repo" do
    {
        "packages" => Package.repository(params[:release], params[:arch], params[:repo])
    }.to_json
end


get "/:release/:arch/:repo/PACKAGES.TXT" do
    @packages = Package.repository(params[:release], params[:arch], params[:repo])
    erb :packages
end


get "/:release/:arch/:repo/:category" do
    {
        params[:category] => Package.category(params[:release], params[:arch], params[:repo], params[:category])
    }.to_json
end


post "/:release/:arch/:repo/:category" do
    data = JSON.parse(request.body.string)
    pkg = Package.new(data)
    pkg.create
    pkg.to_json
end


get "/:release/:arch/:repo/:category/:name" do
    pkg = Package.get(params[:release], params[:arch], params[:repo], params[:name])
    if pkg.empty? then
        status 404
    else
        pkg.to_json
    end
end


put "/:release/:arch/:repo/:category/:name" do
    data = JSON.parse(request.body.string)
    pkg = Package.get(params[:release], params[:arch], params[:repo], params[:name])
    if pkg.empty? then
        status 404
    else
        pkg.update(data)
        pkg.to_json
    end
end


delete "/:release/:arch/:repo/:category/:name" do
    pkg = Package.get(params[:release], params[:arch], params[:repo], params[:name])
    if pkg.empty? then
        status 404
    else
        pkg.delete
        status 204
    end
end


class Package

    @@redis = Redis.new

    def self.redis
        @@redis
    end

    def self.get(release, arch, repo, name)
        k = "#{release}:#{arch}:#{repo}:#{name}"
        Package.new(self.redis.hgetall(k))
    end

    def self.repository(release, arch, repository)
        k = "#{release}:#{arch}:#{repository}"
        pkgs = self.redis.smembers(k)
        pkgs.map { |p| self.redis.hgetall(p) } 
    end

    def self.category(release, arch, repository, category)
        k = "#{release}:#{arch}:#{repository}:#{category}"
        pkgs = self.redis.smembers(k)
        pkgs.map { |p| self.redis.hgetall(p) }
    end

    def initialize(json)
        @json = json
    end

    def redis
        self.class.redis
    end

    def id
        "#{@json['release']}:#{@json['arch']}:#{@json['repository']}:#{@json['name']}"
    end

    def category_id
        "#{@json['release']}:#{@json['arch']}:#{@json['repository']}:#{@json['category']}"
    end

    def repository_id
        "#{@json['release']}:#{@json['arch']}:#{@json['repository']}"
    end

    def create
        self.save
        self.add_to_category
        self.add_to_repository
    end

    def save
        self.validate(@json)
        self.redis.mapped_hmset(self.id, @json)
    end

    def update(json)
        #XXX update sets if repo or category changes
        @json.update(json)
        self.save
    end

    def delete
        self.redis.del(self.id)
        self.remove_from_category
        self.remove_from_repository
        if @json["repository"] != "attic" then
            @json["repository"] = "attic"
            self.save
            self.add_to_category
            self.add_to_repository
        end
    end

    def add_to_category
        self.redis.sadd(self.category_id, self.id)
    end

    def add_to_repository
        self.redis.sadd(self.repository_id, self.id)
    end

    def remove_from_category
        self.redis.srem(self.category_id, self.id)
    end

    def remove_from_repository
        self.redis.srem(self.repository_id, self.id)
    end

    def to_json
        @json.to_json
    end

    def empty?
        @json.empty?
    end

    def validate(json)
        # Raise if invalid
    end
end
